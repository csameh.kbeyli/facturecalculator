package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static kata.facturecalculator.entity.CategorieEnum.LIVRE;

class TvaReduiteCalculatorTest {

    AbstractTaxeCalculator tvaReduiteCalculator = new TvaReduiteCalculator();


    @Test
    void should_apply_tva_reduite_only_when_produit_is_not_importe(){
        Produit produit = new Produit("livre", 12.49, LIVRE, false);
        var result = tvaReduiteCalculator.apply(produit);
        Assertions.assertEquals(1.25, result.getTaxe());
        Assertions.assertEquals(13.74, result.getPrixTTC());
    }

    @Test
    void should_apply_tva_reduite_and_taxe_importation_when_produit_is_importe(){
        Produit produit = new Produit("livre", 12.49, LIVRE, true);
        var result = tvaReduiteCalculator.apply(produit);
        Assertions.assertEquals(1.90, result.getTaxe());
        Assertions.assertEquals(14.39, result.getPrixTTC());

    }


}
