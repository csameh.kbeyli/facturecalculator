package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static kata.facturecalculator.entity.CategorieEnum.AUTRE;

class TvaNormaleCalculatorTest {

    AbstractTaxeCalculator tvaNormaleCalculator = new TvaNormaleCalculator();

    @Test
    void should_arrondir_au_5_centime_superieur(){



        double taxe = tvaNormaleCalculator.arrondirAu5CentimeSuperieur(0.95);
        double taxe2 = tvaNormaleCalculator.arrondirAu5CentimeSuperieur( 0.99);
        double taxe3 = tvaNormaleCalculator.arrondirAu5CentimeSuperieur(1.00);
        double taxe4 = tvaNormaleCalculator.arrondirAu5CentimeSuperieur(1.01);
        double taxe5 = tvaNormaleCalculator.arrondirAu5CentimeSuperieur(1.02);

        Assertions.assertEquals(0.95,taxe);
        Assertions.assertEquals(1.00, taxe2);
        Assertions.assertEquals(1.00, taxe3);
        Assertions.assertEquals(1.05, taxe4);
        Assertions.assertEquals(1.05, taxe5);

    }


    @Test
    void should_not_apply_taxe_importation_when_produit_is_not_importe(){
        Produit produit = new Produit("CD musical", 14.99, AUTRE, false);
        var result = tvaNormaleCalculator.apply(produit);
        Assertions.assertEquals(3.0, result.getTaxe());
        Assertions.assertEquals(17.99, result.getPrixTTC());
    }

    @Test
    void should_apply_taxe_importation_when_produit_is_importe(){
        Produit produit = new Produit("CD musical", 14.99, AUTRE, true);
        var result = tvaNormaleCalculator.apply(produit);
        Assertions.assertEquals(3.75, result.getTaxe());
        Assertions.assertEquals(18.74, result.getPrixTTC());

    }

}
