package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static kata.facturecalculator.entity.CategorieEnum.PREMIERES_NECESSITES;

class SansTvaCalculatorTest {

    AbstractTaxeCalculator sansTvaCalculator = new SansTvaCalculator();

    @Test
    void should_not_apply_any_tva_when_produit_is_not_importe() {
        Produit produit = new Produit("chocolat", 0.85, PREMIERES_NECESSITES, false);
        var result = sansTvaCalculator.apply(produit);
        Assertions.assertEquals(0.0, result.getTaxe());
        Assertions.assertEquals(0.85, result.getPrixTTC());
    }

    @Test
    void should_apply_taxe_importation_when_produit_is_importe() {
        Produit produit = new Produit("chocolat", 0.85, PREMIERES_NECESSITES, true);
        var result = sansTvaCalculator.apply(produit);
        Assertions.assertEquals(0.05, result.getTaxe());
        Assertions.assertEquals(0.90, result.getPrixTTC());
    }
}
