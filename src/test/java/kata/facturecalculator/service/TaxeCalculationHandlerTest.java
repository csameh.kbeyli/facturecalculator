package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static kata.facturecalculator.entity.CategorieEnum.AUTRE;
import static kata.facturecalculator.entity.CategorieEnum.LIVRE;
import static kata.facturecalculator.entity.CategorieEnum.PREMIERES_NECESSITES;

class TaxeCalculationHandlerTest {

    private static final TaxeCalculationHandler taxesCalculatorChain = new TaxeCalculationHandler();


    @Test
    void should_not_apply_taxe_importation_when_produit_category_is_other(){
        Produit produit = new Produit("CD musical", 14.99, AUTRE, false);
        var result = taxesCalculatorChain.apply(produit);
        Assertions.assertEquals(3.0, result.getTaxe());
        Assertions.assertEquals(17.99, result.getPrixTTC());
    }

    @Test
    void should_apply_tva_reduite_only_when_produit_category_is_book(){
        Produit produit = new Produit("livre", 12.49, LIVRE, false);
        var result = taxesCalculatorChain.apply(produit);
        Assertions.assertEquals(1.25, result.getTaxe());
        Assertions.assertEquals(13.74, result.getPrixTTC());
    }


    @Test
    void should_not_apply_any_tva_when_produit_category_is_primary_needs() {
        Produit produit = new Produit("chocolat", 0.85, PREMIERES_NECESSITES, false);
        var result = taxesCalculatorChain.apply(produit);
        Assertions.assertEquals(0.0, result.getTaxe());
        Assertions.assertEquals(0.85, result.getPrixTTC());
    }

}
