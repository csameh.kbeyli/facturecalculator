package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;

import static kata.facturecalculator.entity.CategorieEnum.AUTRE;

public class TvaNormaleCalculator extends AbstractTaxeCalculator {
    private static final double TAUX_TVA_NORMALE = 20.0;

    @Override
    protected boolean accept(Produit produit) {
        return AUTRE.equals(produit.getCategorie());
    }

    @Override
    public Produit apply(Produit produit) {
        double tauxTaxTotal = produit.isImporte()? TAUX_TAXE_IMPORTATION + TAUX_TVA_NORMALE: TAUX_TVA_NORMALE;
        double tax = arrondirAu5CentimeSuperieur(produit.getPrixHorsTaxes() * tauxTaxTotal / 100);
        produit.setTaxe(tax);
        return produit;
    }
}
