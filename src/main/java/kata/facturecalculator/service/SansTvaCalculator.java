package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;

import static kata.facturecalculator.entity.CategorieEnum.PREMIERES_NECESSITES;

public class SansTvaCalculator extends AbstractTaxeCalculator {

    @Override
    protected boolean accept(Produit produit) {
        return PREMIERES_NECESSITES.equals(produit.getCategorie());
    }

    @Override
    public Produit apply(Produit produit) {
        double tauxTaxTotal = produit.isImporte()? TAUX_TAXE_IMPORTATION: 0;
        double taxe = arrondirAu5CentimeSuperieur(produit.getPrixHorsTaxes() * tauxTaxTotal / 100);
        produit.setTaxe(taxe);
        return produit;
    }
}
