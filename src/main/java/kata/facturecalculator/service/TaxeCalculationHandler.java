package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class TaxeCalculationHandler implements UnaryOperator<Produit> {

    private static final Supplier<Stream<AbstractTaxeCalculator>> calculatorsStream = () -> Stream.of(new TvaReduiteCalculator(), new TvaNormaleCalculator(), new SansTvaCalculator());

    @Override
    public Produit apply(Produit produit) {
        return calculatorsStream.get().reduce(produit, (input, calculator) -> calculator.calculerTaxe(produit), (identity, newValue) -> newValue);
    }
}
