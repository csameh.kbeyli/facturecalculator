package kata.facturecalculator.service;

import kata.facturecalculator.entity.Achat;
import kata.facturecalculator.entity.Panier;

import java.util.List;

public class PanierService {

    private static final TaxeCalculationHandler taxesCalculatorChain = new TaxeCalculationHandler();

    public void imprimerFacture(Panier panier) {

        List<Achat> achatsAvecPrixTTC = panier.getAchats().stream()
                .map(achat -> new Achat(achat.quantite(), taxesCalculatorChain.apply(achat.produit())))
                .toList();

        double montantTotalTTC = calculerMontantTotalTTC(achatsAvecPrixTTC);
        double montantTotalTaxes = calculerMontantTotalTaxes(achatsAvecPrixTTC);

        System.out.println("#### Output");
        panier.getAchats().forEach(achat -> System.out.println(achat.outputToString()));

        System.out.println("\nMontant des taxes : " + Math.floor(montantTotalTaxes * 100) / 100 + " €");
        System.out.println("\nTotal : " + Math.floor(montantTotalTTC * 100) / 100 + " €");
    }

    private double calculerMontantTotalTTC(List<Achat> achats) {
        return achats.stream().map(achat -> achat.quantite() * achat.produit().getPrixTTC())
                .reduce(0.0, Double::sum);
    }

    private double calculerMontantTotalTaxes(List<Achat> achats) {
        return achats.stream().map(achat -> achat.quantite() * achat.produit().getTaxe())
                .reduce(0.0, Double::sum);
    }
}
