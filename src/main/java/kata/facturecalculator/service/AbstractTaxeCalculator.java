package kata.facturecalculator.service;

import kata.facturecalculator.entity.Produit;

import java.util.function.UnaryOperator;

public abstract class AbstractTaxeCalculator implements UnaryOperator<Produit> {

    protected final double TAUX_TAXE_IMPORTATION = 5.0;

    public double arrondirAu5CentimeSuperieur(double montant) {

        double montantArrondiSuperieur = Math.ceil(montant * 20) / 20; // Montant arrondi supérieur

        double differenceSuperieur = montantArrondiSuperieur - montant;

        if (differenceSuperieur < 0.05) {
            return montantArrondiSuperieur;
        } else {
            return montant;
        }
    }

    protected abstract boolean accept(Produit produit);

    Produit calculerTaxe(Produit produit) {
        return accept(produit) ? apply(produit) : produit;
    }
}
