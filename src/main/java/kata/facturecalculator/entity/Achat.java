package kata.facturecalculator.entity;

public record Achat(Integer quantite, Produit produit) {

    public double prixTTC() {
        return quantite * produit().getPrixTTC();
    }

    public String inputToString() {
        return "* " + quantite() + " " + produit().getNom() + " à " + produit().getPrixHorsTaxes() + "€";
    }

    public String outputToString() {
        return inputToString() + " : " + String.format("%.2f", prixTTC()) + "€ TTC";
    }
}
