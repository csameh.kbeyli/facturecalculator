package kata.facturecalculator.entity;

import java.util.ArrayList;
import java.util.List;

public class Panier {
    private List<Achat> achats;

    public Panier() {

        achats = new ArrayList<Achat>();
    }

    public List<Achat> getAchats() {
        return achats;
    }

    public void ajouterAchat(Achat achat) {
        achats.add(achat);
    }
}

