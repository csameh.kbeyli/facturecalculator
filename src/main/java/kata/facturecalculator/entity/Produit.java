package kata.facturecalculator.entity;

public class Produit {
    private String nom;

    private CategorieEnum categorie;

    private boolean importe;

    private double prixHorsTaxes;

    private double taxe;

    public Produit(String nom, double prixHorsTaxes, CategorieEnum categorie, boolean importe) {
        this.nom = nom;
        this.prixHorsTaxes = prixHorsTaxes;
        this.categorie = categorie;
        this.importe = importe;
    }

    public double getTaxe() {
        return taxe;
    }

    public void setTaxe(double taxe) {
        this.taxe = taxe;
    }

    public CategorieEnum getCategorie() {
        return categorie;
    }

    public double getPrixHorsTaxes() {
        return prixHorsTaxes;
    }

    public String getNom() {
        return nom;
    }

    public boolean isImporte() {
        return importe;
    }

    public double getPrixTTC() {

        return Math.floor((prixHorsTaxes + taxe) * 100) / 100;
    }

    public String toString() {
        return nom + " : " + getPrixTTC() + "€ TTC";
    }
}
