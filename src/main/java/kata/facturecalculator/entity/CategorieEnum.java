package kata.facturecalculator.entity;

public enum CategorieEnum {

    PREMIERES_NECESSITES,
    LIVRE,
    AUTRE,
}
