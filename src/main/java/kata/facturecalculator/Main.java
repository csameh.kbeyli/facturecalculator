package kata.facturecalculator;

import kata.facturecalculator.entity.Achat;
import kata.facturecalculator.entity.Panier;
import kata.facturecalculator.entity.Produit;
import kata.facturecalculator.service.PanierService;

import static kata.facturecalculator.entity.CategorieEnum.AUTRE;
import static kata.facturecalculator.entity.CategorieEnum.LIVRE;
import static kata.facturecalculator.entity.CategorieEnum.PREMIERES_NECESSITES;

public class Main {

    public static void main(String[] args) {

        PanierService panierService = new PanierService();

        Panier panier1 = new Panier();
        panier1.ajouterAchat(new Achat(2, new Produit("livres", 12.49, LIVRE, false)));
        panier1.ajouterAchat(new Achat(1, new Produit("CD musical", 14.99, AUTRE, false)));
        panier1.ajouterAchat(new Achat(3,new Produit(" barres de chocolat", 0.85, PREMIERES_NECESSITES, false)));


        Panier panier2 = new Panier();
        panier2.ajouterAchat(new Achat(2, new Produit("boîtes de chocolats importée", 10.0, PREMIERES_NECESSITES, true)));
        panier2.ajouterAchat(new Achat(3, new Produit("flacons de parfum importé", 47.50, AUTRE, true)));

        Panier panier3 = new Panier();
        panier3.ajouterAchat(new Achat(2, new Produit("flacons de parfum importés", 27.99, AUTRE, true)));
        panier3.ajouterAchat(new Achat(1, new Produit("flacons de parfum", 18.99, AUTRE, false)));
        panier3.ajouterAchat(new Achat(3, new Produit("boîtes de pilules contre la migraine", 9.75, PREMIERES_NECESSITES, false)));
        panier3.ajouterAchat(new Achat(2, new Produit("boîtes de chocolats importées", 11.25, PREMIERES_NECESSITES, true)));


        System.out.println("\n\n### Panier 1");
        System.out.println("#### Input");
        panier1.getAchats().forEach(achat -> System.out.println(achat.inputToString()));
        panierService.imprimerFacture(panier1);

        System.out.println("\n\n### Panier 2");
        System.out.println("#### Input");
        panier2.getAchats().forEach(achat -> System.out.println(achat.inputToString()));
        panierService.imprimerFacture(panier2);

        System.out.println("\n\n### Panier 3");
        System.out.println("#### Input");
        panier3.getAchats().forEach(achat -> System.out.println(achat.inputToString()));
        panierService.imprimerFacture(panier3);

    }
}
